# OSES ASSIGNMENT ON EMBEEDED LINUX
# Heart Rate Monitor
## How to create the layer

*bitbake-layers create-layer ../meta-assign*

from the poky build, /poky/build_rpi3 for raspberry 3 or poky/build_qemuarm.

*bitbake-layers add-layer ../meta-assign/*

to add the layer on the image configuration.

## How to create the recipe

First clone the repository

*git clone <-url->*

Also you should append on local/local.conf of your implementation this line for the Linux user application (APP)

*IMAGE_INSTALL_append = " heartbeat"*

and for the Linux character-based driver (cDD)

*IMAGE_INSTALL_append = " driverel"*

*KERNEL_MODULE_AUTOLOAD += " driverel"*

after that 

*bitbake core-image-minimal* for qemuarm

or 

*bitbake core-image-full-cmdline* for raspberry

remember to put on driverel.bb the compatibility
## How to manage raspberry

Know you need to set-up the micro-sd with our image for raspberry

*sudo dd if=tmp/deploy/images/raspberrypi3/core-image-full-cmdline-raspberrypi3.rpi-sdimg of=/dev/sdb bs=1M*
