#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include "data.h"

static dev_t driverel_dev;

struct cdev driverel_cdev;

static char buffer[64];

struct class *myclass = NULL;

ssize_t driverel_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
    printk(KERN_INFO "[driverel]read (count=%d, ppg=%d)\n", (int)count, ppg[(int)count]);
    
    return ppg[(int)count];
}

struct file_operations driverel_fops = {
    .owner = THIS_MODULE,
    .read = driverel_read,
};

static int __init driverel_module_init(void)
{
    printk(KERN_INFO "Loading driverel_module\n");
    
    alloc_chrdev_region(&driverel_dev, 0, 1, "driverel_dev");
    printk(KERN_INFO "%s\n", format_dev_t(buffer, driverel_dev));
    
    myclass = class_create(THIS_MODULE, "driverel_sys");
    device_create(myclass, NULL, driverel_dev, NULL, "driverel");
    
    cdev_init(&driverel_cdev, &driverel_fops);
    driverel_cdev.owner = THIS_MODULE;
    cdev_add(&driverel_cdev, driverel_dev, 1);
    
    return 0;
}

static void __exit driverel_module_cleanup(void)
{
    printk(KERN_INFO "Cleaning-up driverel_dev.\n");
    
    cdev_del(&driverel_cdev);
    unregister_chrdev_region(driverel_dev, 1);
}

module_init(driverel_module_init);
module_exit(driverel_module_cleanup);

MODULE_AUTHOR("Dario Licastro");
MODULE_LICENSE("GPL");
