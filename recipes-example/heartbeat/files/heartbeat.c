#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>

#define q	11		    /* for 2^11 points */
#define N	(1<<q)		/* N-point FFT, iFFT */

typedef float real;
typedef struct{real Re; real Im;} complex;
complex vett[N];
int fd = -1;

#ifndef PI
# define PI	3.14159265358979323846264338327950288
#endif


void fft( complex *v, int n, complex *tmp )
{
  if(n>1) {			/* otherwise, do nothing and return */
    int k,m;    complex z, w, *vo, *ve;
    ve = tmp; vo = tmp+n/2;
    for(k=0; k<n/2; k++) {
      ve[k] = v[2*k];
      vo[k] = v[2*k+1];
    }
    fft( ve, n/2, v );		/* FFT on even-indexed elements of v[] */
    fft( vo, n/2, v );		/* FFT on odd-indexed elements of v[] */
    for(m=0; m<n/2; m++) {
      w.Re = cos(2*PI*m/(double)n);
      w.Im = -sin(2*PI*m/(double)n);
      z.Re = w.Re*vo[m].Re - w.Im*vo[m].Im;	/* Re(w*vo[m]) */
      z.Im = w.Re*vo[m].Im + w.Im*vo[m].Re;	/* Im(w*vo[m]) */
      v[  m  ].Re = ve[m].Re + z.Re;
      v[  m  ].Im = ve[m].Im + z.Im;
      v[m+n/2].Re = ve[m].Re - z.Re;
      v[m+n/2].Im = ve[m].Im - z.Im;
    }
  }
  return;
}

void FFTPSD(complex v[N]){
	  complex scratch[N];
	  float abs[N];
	  int k;
	  int m;
	  int i;
	  int minIdx, maxIdx;

	  printf("FFTPSD");
	// FFT computation
	  fft( v, N, scratch );

	// PSD computation
	  for(k=0; k<N; k++) {
		abs[k] = (50.0/2048)*((v[k].Re*v[k].Re)+(v[k].Im*v[k].Im));
	  }

	  minIdx = (0.5*2048)/50;   // position in the PSD of the spectral line corresponding to 30 bpm
	  maxIdx = 3*2048/50;       // position in the PSD of the spectral line corresponding to 180 bpm

	// Find the peak in the PSD from 30 bpm to 180 bpm
	  m = minIdx;
	  for(k=minIdx; k<(maxIdx); k++) {
	    if( abs[k] > abs[m] )
		m = k;
	  }

	// Print the heart beat in bpm
	  printf( "\n\n\n%d bpm\n\n\n", (m)*60*50/2048 );
}

void * changeVett (){
	complex v[N];
	int i;
	for (i=0;i<N;++i){
		v[i].Im=vett[i].Im;
		v[i].Re=vett[i].Re;
	}
	FFTPSD(v);
	pthread_exit(NULL);
}

void * funread (int* c){
	int d;
	d = read( fd, 0, *c );
	vett[*c].Re= d;
	vett[*c].Im=0.0;
	printf( "read: %f\n", vett[*c].Re );
	pthread_exit(NULL);
}

int main(int argc, char **argv)
{
  char *app_name = argv[0];
  char *dev_name = "/dev/driverel";
  int i;
  pthread_t reading[N], fftthread;
  //clock_t first,second;

	if ((fd = open(dev_name, O_RDWR)) < 0)
	{
		fprintf(stderr, "%s: unable to open %s: %s\n", app_name, dev_name, strerror(errno));
		return( 1 );
	}
	read( fd, 0, 0 );

	for ( i=0 ; i<N ; ++i){
		reading[i]=i;
	}

	while(1){
		//first=clock();
		for( i=0 ; i<N ; ++i){
			pthread_create(&reading[i], NULL, funread,(void *) &i);
			pthread_detach(reading[i]);
			usleep(20000);
			/*second=clock();
			usleep(20000-((second-first)*50/CLOCKS_PER_SEC));
			printf( "ops: %f\n", ((second-first)*50/CLOCKS_PER_SEC));
			first=second;*/ // The execution time is less then microseconds both with qemuarm and raspberry
		}
		pthread_create(&fftthread, NULL, changeVett, NULL);
		pthread_detach(fftthread);
		//sleep(15);
	}
  close( fd );
  exit(EXIT_SUCCESS);
}

